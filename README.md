 ## Run code ##
  *sequential.c*
    
    Compile
        gcc sequential.c -o sequential
    Run
        ./sequential arguments1 arguments2 arguments3

  *parallel.c*

    Compile
        mpicc parallel.c -o parallel
    Run
        mpiexec -np [number process] ./parallel arguments1 arguments2 arguments
    